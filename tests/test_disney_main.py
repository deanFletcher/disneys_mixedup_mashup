import json
import os
from unittest.mock import patch, MagicMock

import pytest
from PIL import Image
from click.testing import CliRunner

from pythonProject.disneys_mixedup_mashup.code.main import get_single_character_byName, \
    get_random_character_byID, store_image, NotFoundException, mash_images, cli

mock_data = {'info': {'count': 1, 'totalPages': 1, 'previousPage': None, 'nextPage': None}, 'data': {'_id': 3082,
                                                                                                     'allies': [],
                                                                                                     'enemies': [],
                                                                                                     'sourceUrl': 'https://disney.fandom.com/wiki/Hercules_(character)',
                                                                                                     'name': 'Hercules',
                                                                                                     'imageUrl': 'https://static.wikia.nocookie.net/disney/images/7/70/Profile_-_Hercules.jpeg',
                                                                                                     'createdAt': '2021-04-12T02:11:03.200Z',
                                                                                                     'updatedAt': '2021-12-20T20:39:48.138Z',
                                                                                                     'url': 'https://api.disneyapi.dev/characters/3082',
                                                                                                     '__v': 0}}
def test_get_single_character_byName_found():
    assert get_single_character_byName('hercules') == '../image_files/raw_photos/hercules.jpg'


def test_get_single_character_byName_not_found():
    with pytest.raises(NotFoundException) as error:
        get_single_character_byName('spok')
    assert str(error.value) == 'Whoops! Looks like "spok" is not the correct name for any Disney characters!'


def test_get_random_character_byId():
    mock_response = MagicMock()
    mock_response.content = json.dumps(mock_data).encode('utf-8')
    mock_response.json.return_value = mock_data
    with patch('requests.get', return_value=mock_response):
        with patch('pythonProject.disneys_mixedup_mashup.code.main.store_image',
                   return_value='../image_files/raw_photos/hercules.jpg'):
            with patch('random.randint', return_value=3082):
                # Call the function to test
                img_path, char_name = get_random_character_byID()
    assert img_path == '../image_files/raw_photos/hercules.jpg'
    assert char_name == 'Hercules'


def test_store_image(tmp_path):
    # Mock character data
    mock_character = {
        'imageUrl': 'http://example.com/image.jpg',
        'name': 'Hercules'
    }
    img_folder = tmp_path / 'image_files' / 'raw_photos'
    img_folder.mkdir(parents=True, exist_ok=True)
    with patch('requests.get') as mock_get:
        with open('test_pic1.jpg', 'rb') as img_file:
            valid_image_data = img_file.read()
        mock_get.return_value = MagicMock(content=valid_image_data)
        with patch.dict('os.environ', {'IMG_FOLDER': str(img_folder)}):
            store_image(mock_character)
            saved_image_path = img_folder / 'Hercules.jpg'
            assert saved_image_path.is_file()


def test_mash_images_returns_correct_path():
    char1_path = 'test_pic1.jpg'
    char2_path = 'test_pic2.jpg'
    mash_name = 'test_mash'
    expected_path = '../image_files/converted_photos/test_mash.png'
    assert mash_images(char1_path, char2_path, mash_name) == expected_path


def test_mash_images_creates_file():
    char1_path = 'test_pic1.jpg'
    char2_path = 'test_pic2.jpg'
    mash_name = 'test_mash'
    mashed_path = mash_images(char1_path, char2_path, mash_name)
    assert os.path.isfile(mashed_path)


def test_mash_images_correct_size():
    char1_path = 'test_pic1.jpg'
    char2_path = 'test_pic2.jpg'
    mash_name = 'test_mash'
    target_size = (500, 500)
    mashed_path = mash_images(char1_path, char2_path, mash_name)
    with Image.open(mashed_path) as img:
        assert img.size == target_size


def test_mash_images_correct_mode():
    char1_path = 'test_pic1.jpg'
    char2_path = 'test_pic2.jpg'
    mash_name = 'test_mash'
    mashed_path = mash_images(char1_path, char2_path, mash_name)
    with Image.open(mashed_path) as img:
        assert img.mode == 'RGBA'


def mock_get_single_character_byName(name):
    if name == "Mickey":
        return "Mickey Mouse"
    elif name == "Elsa":
        return "Elsa"
    else:
        return "Character not found"


@pytest.fixture
def mock_get_character(monkeypatch):
    monkeypatch.setattr('pythonProject.disneys_mixedup_mashup.code.main.get_single_character_byName',
                        mock_get_single_character_byName)


# Write the test using the mock
def test_get_character_by_name(mock_get_character):
    runner = CliRunner()
    result = runner.invoke(cli, ['get-character-by-name', '--name', 'Mickey Mouse'])
    assert result.output in "mickey_mouse.jpg"
    result = runner.invoke(cli, ['get-character-by-name', '--name', 'Unknown'])
    assert result.output in ""