import json
import os
import random
from io import BytesIO
import click

import requests
from PIL import Image

DISNEY_URL = 'https://api.disneyapi.dev/character'
TARGET_SIZE = (500, 500)
IMG_FOLDER = os.getenv('IMG_FOLDER', '../image_files/raw_photos')


class InvalidCharacterNameError(Exception):
    """Exception raised when an invalid character name is provided."""
    pass
class NotFoundException(Exception):
    def __init__(self, value):
        self.value = value


def validate_response(response):
    """Validate the API response."""
    if not response.ok:
        raise requests.RequestException(f"API request failed with status code {response.status_code}")
    try:
        breakpoint()
        data = response.json()
    except json.JSONDecodeError:
        raise ValueError("Response content is not valid JSON")
    if 'data' not in data or not data['data']:
        raise InvalidCharacterNameError("No character data found")
    return data['data']


def get_single_character_byName(char_name):
    params = {'name': char_name.lower()}
    try:
        response = requests.get(url=DISNEY_URL, params=params)
        char_response = json.loads(response.content)
        character_data = char_response['data']
        if character_data:
            if type(character_data) is dict:
                return process_character_data(character_data, char_name)
            else:
                for data in character_data:
                    if data.get('name').lower() == char_name:
                        return process_character_data(data, char_name)

        else:
            raise NotFoundException(f'Whoops! Looks like "{char_name}" is not the correct name for any Disney '
                                    f'characters!')
    except TypeError as e:
        raise TypeError('Character Name must be string:', e)


def process_character_data(char_data, char_name):
    img_path = store_image(char_data)
    img = Image.open(img_path)
    img.show()
    click.echo(f'Here is your Disney character: {char_name}!')
    return img_path


@click.group()
def cli():
    """Welcome to Disney's Mixed-Up Mash-Up!"""
    pass


@cli.command()
@click.option("--name", prompt="Enter a Disney Character's Name", help="The Name of a Disney Character as string")
def get_character_by_name(name):
    """Get a single Disney character by name."""
    get_single_character_byName(name)


def get_random_character_byID():
    while True:
        char_id = random.randint(1, 7400)
        response = requests.get(url=DISNEY_URL + f'/{char_id}')
        char_response = json.loads(response.content)
        character_data = char_response['data']
        char_name = character_data.get('name')
        if character_data:
            img_path = store_image(character_data)
            img = Image.open(img_path)
            img.show()
            click.echo(f'Wow, it is your lucky day! Your new best bud is {char_name}!')
            return img_path, char_name
        else:
            click.echo(f'Whoops! Looks like I goofed up! Let us try that again.')


@cli.command()
def get_random_character():
    """Randomly get a Disney character."""
    get_random_character_byID()


def store_image(character):
    char_img_url = character.get('imageUrl')
    char_name = character.get('name')
    img_name = char_name.replace(' ', '_').lower()
    img_response = requests.get(char_img_url)
    img = Image.open(BytesIO(img_response.content))
    img_folder = os.getenv('IMG_FOLDER', '../image_files/raw_photos')
    img_path = f'{img_folder}/{img_name}.jpg'
    if img.mode == 'RGBA':
        img = img.convert('RGB')
    img.save(img_path, 'JPEG')
    return img_path


def mash_images(char1_path, char2_path, mash_name):
    img1 = Image.open(char1_path)
    img2 = Image.open(char2_path)
    img1_resize = img1.resize(TARGET_SIZE)
    img2_resize = img2.resize(TARGET_SIZE)
    # Adjust transparency from 0 to 255
    img1_resize.putalpha(255)
    img2_resize.putalpha(150)
    # Overlay the images
    overlay_image = Image.new('RGBA', TARGET_SIZE)
    overlay_image.paste(img1_resize, (0, 0), img1_resize)
    overlay_image.paste(img2_resize, (0, 0), img2_resize)
    mashed_name = mash_name.replace(' ', '_').lower()
    mashed_path = f'../image_files/converted_photos/{mashed_name}.png'
    overlay_image.save(mashed_path)
    return mashed_path


def validate_user_input(value):
    if (value.lower() != 'i pick') and (value.lower() != 'you pick'):
        raise click.BadParameter('Tsk, Tsk.  You have to type "I Pick" or "You Pick"')
    return value


@cli.command()
def mashup():
    """Create a mashup of two Disney characters."""
    click.echo(
        "It's Mash-Up Time!!! \nThis Magical program will take your two favorite Disney characters and combined them! \nResults may be delightful, disturbing, or something in between.")
    random_char = click.prompt(
        '\nWould you like to select your own Disney characters to Mash or let me pick them for you? \nRespond with "I pick" or "You pick" ',
        type=str, value_proc=validate_user_input)
    img1_path = None
    img2_path = None
    char1_name = None
    char2_name = None
    if random_char.lower() == "i pick":
        while True:
            char1_name = click.prompt('\nWhat is the name of the first Disney Character you choose? ', type=str)
            try:
                img1_path = get_single_character_byName(char1_name)
                break  # If successful, break out of the loop
            except Exception as e:
                click.echo(f'An error occurred: {e} Please try again.')

        while True:
            char2_name = click.prompt('\nWhat is the name of the second Disney Character you choose? ', type=str)
            try:
                img2_path = get_single_character_byName(char2_name)
                break  # If successful, break out of the loop
            except Exception as e:
                click.echo(f'An error occurred: {e} Please try again.')

    elif random_char.lower() == "you pick":
        img1_path, char1_name = get_random_character_byID()
        img2_path, char2_name = get_random_character_byID()

    mash_name = click.prompt('\nWhat do you want your Mashed-up Character to be named? ', type=str)
    mashed_image_path = mash_images(img1_path, img2_path, mash_name)
    mashed_image = Image.open(mashed_image_path)
    mashed_image.show()
    click.echo(f'Mish, Mash, Mush: Mixed together {char1_name} & {char2_name} to create {mash_name}!')


if __name__ == '__main__':
    cli()
