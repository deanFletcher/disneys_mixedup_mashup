# Disneys_MixedUp_MashUp



## Getting started

Welcome!  Disney's Mixed-Up Mash-Up is a fun new Command Line Interface game that allows you to get creative with classic and modern Disney Characters! 
 Hopefully this ReadME guides you through the steps of the game. 
## How to Begin

- The first step is to cd into the 'disneys_mixedup_mash' file and create and activate your virtual environment
- Install the requirements.txt file
- Then CD into the 'code' folder to find the 'main.py' file.
- Finally, begin the game by entering the command 'python main.py'.  And you're off!

```commandline
cd /python_capstone/pythonProject/disneys_mixedup_mashup/
using gitbash, create venv: python -m venv [yourEnvNameHere]
activate your venv:  source [yourEnvNameHere]/Scripts/activate
pip install requirements.txt
cd 'code'
python main.py
```

## Playing the Game

- Upon execution, the CLI game menu will appear, providing you with a list of three commands:
1) get-character-by-name --- Get a photo of a single disney character using a string word search
2) get-random-character  --- The computer will randomly grab one of 7,400+ Disney characters and return the photo
3) mashup                --- Create a mashup photo of any two disney characters


## Get Character by Name

Using the CLI, enter the command:
```commandline
python main.py get-character-by-name
Enter a Disney Character's Name: [Name Goes Here]

```
- This will prompt you to enter the String name of any disney character you choose. 
- Please try to use their full name, such as "Mickey Mouse" instead of "Mickey" to ensure you get the correct character the first time.
- The service is usually smart enough to find partial searches, but it will only return the first option found, not a list of all matching characters.
- The program is not case sensitive, but if the character name has multiple words, you must use a space as well.
- Upon entering a valid disney character name, the service will return the character and open up a photo of the character.

![drunkMickey.png.png](..%2F..%2F..%2F..%2FOneDrive%2FPictures%2FdrunkMickey.png.png)
(This is a personal favorite Mickey pic of mine, but the service does not retrieve this one specifically. Sad! )
***

# Get Random Character

This one is easy!  Just run the command and the program will randomly grab one of 7400 listed disney characters, and display their photo

```commandline
python main.py get-random-character
Wow, it is your lucky day! Your new best bud is Gilbert!
```
- When I say random character, I mean RANDOM.  There are some really odd and obscure characters on this database, from any disney movie, tv show, video game, etc. You will learn alot about the underbelly of Disney this way.
![doug.jpg](image_files%2Fraw_photos%2Fdoug.jpg)
- (Oh Yes, it's Doug.  Of course it is)
# It's Mashup Time!

Now the Mashup command.  This combines the functionality of get-random-character and get-character-by-name and adds a little twist.:

- You can either select 'I Pick', which will enable you to enter the name of two disney characters, or 'You pick', which will prompt the computer to grab two disney characters at random.
- After entering two names or getting two at random, the computer will then prompt you to create a new name for your Mashed Up creation.
- The computer will then merge the two photos by making them the same file type, the same size, making one partially transparent, and then pasting it ontop of the other photo.  Your mashup is complete!
### You Pick or I Pick
```commandline
It's Mash-Up Time!!!
This Magical program will take your two favorite Disney characters and combined them!
Results may be delightful, disturbing, or something in between.

Would you like to select your own Disney characters to Mash or let me pick them for you?
Respond with "I pick" or "You pick" : I pick

What is the name of the first Disney Character you choose? : mickey mouse
Here is your Disney character: mickey mouse!

What is the name of the second Disney Character you choose? : hercules
Here is your Disney character: hercules!

What do you want your Mashed-up Character to be named? : mercules
Mish, Mash, Mush: Mixed together mickey mouse & hercules to create mercules!

```
![mercules.png](image_files%2Fconverted_photos%2Fmercules.png)
(Behold, the might Mercules!)

# Wrapping Up
- If you want to play again, just run the python main.py command!

# The Messy Details
- For this project, we call the disneyapi.dev, which does NOT need an API key. 
https://disneyapi.dev/docs/

### Dependencies:
This is the list of libraries and imports that must be used.
They are stored in the requirements.txt file
1) import json
2) import os
3) import random
4) from io import BytesIO
5) import click
6) import requests
7) from PIL import Image